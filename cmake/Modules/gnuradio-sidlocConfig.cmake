find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_SIDLOC gnuradio-sidloc)

FIND_PATH(
    GR_SIDLOC_INCLUDE_DIRS
    NAMES gnuradio/sidloc/api.h
    HINTS $ENV{SIDLOC_DIR}/include
        ${PC_SIDLOC_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_SIDLOC_LIBRARIES
    NAMES gnuradio-sidloc
    HINTS $ENV{SIDLOC_DIR}/lib
        ${PC_SIDLOC_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-sidlocTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_SIDLOC DEFAULT_MSG GR_SIDLOC_LIBRARIES GR_SIDLOC_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_SIDLOC_LIBRARIES GR_SIDLOC_INCLUDE_DIRS)
