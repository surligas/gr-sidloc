/* -*- c++ -*- */
/* gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_SPREADING_IMPL_H
#define INCLUDED_SIDLOC_SPREADING_IMPL_H

#include <gnuradio/sidloc/spreading.h>

namespace gr {
namespace sidloc {

template <typename T>
class spreading_impl : public spreading<T>
{
private:
    const bool m_interleave;
    const T m_scale;
    code::sptr m_code;

    T
    scale_bit(bool b);
public:
    spreading_impl(code::sptr c, T scale, bool channel_interleaving);
    ~spreading_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_SPREADING_IMPL_H */
