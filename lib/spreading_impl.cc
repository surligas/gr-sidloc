/* -*- c++ -*- */
/* gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spreading_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace sidloc {

template <typename T>
typename spreading<T>::sptr
spreading<T>::make(code::sptr c, T scale, bool channel_interleaving)
{
    return gnuradio::make_block_sptr<spreading_impl<T>>(c, scale, channel_interleaving);
}

spreading<uint8_t>::sptr spreading<uint8_t>::make(code::sptr c, bool channel_interleaving)
{
    return gnuradio::make_block_sptr<spreading_impl<uint8_t>>(c, 1, channel_interleaving);
}


/*
 * The private constructor
 */
template <typename T>
spreading_impl<T>::spreading_impl(code::sptr c, T scale, bool channel_interleaving)
    : gr::sync_block("spreading",
                     gr::io_signature::make(0, 0, 0),
                     gr::io_signature::make(channel_interleaving ? 1 : c->channels(),
                                            channel_interleaving ? 1 : c->channels(),
                                            sizeof(T))),
      m_interleave(channel_interleaving),
      m_scale(scale),
      m_code(c)

{
    this->message_port_register_in(pmt::mp("pdu"));
}

/*
 * Our virtual destructor.
 */
template <typename T>
spreading_impl<T>::~spreading_impl()
{
}

template <typename T>
T spreading_impl<T>::scale_bit(bool b)
{
    /* Cast it to integer so we can balance it around 0 */
    ssize_t x = b;
    x = 2 * x - 1;
    return x * m_scale;
}

template <>
gr_complex spreading_impl<gr_complex>::scale_bit(bool b)
{
    /* Cast it to integer so we can balance it around 0 */
    gr_complex x(b ? 1.0f : -1.0f, 0);
    return x * m_scale;
}

template <>
uint8_t spreading_impl<uint8_t>::scale_bit(bool b)
{
    return b;
}

template <typename T>
int spreading_impl<T>::work(int noutput_items,
                            gr_vector_const_void_star& input_items,
                            gr_vector_void_star& output_items)
{
    std::vector<bool> x(m_code->channels());
    if (m_interleave) {
        auto out = static_cast<T*>(output_items[0]);
        for (int i = 0; i < noutput_items / m_code->channels(); i++) {
            m_code->next(x);
            for (size_t j = 0; j < m_code->channels(); j++) {

                out[i * m_code->channels() + j] = scale_bit(x[j]);
            }
        }
        return (noutput_items / m_code->channels()) * m_code->channels();
    } else {
        for (int i = 0; i < noutput_items; i++) {
            m_code->next(x);
            for (size_t j = 0; j < m_code->channels(); j++) {
                auto out = static_cast<T*>(output_items[j]);
                out[i] = scale_bit(x[j]);
            }
        }
    }
    return noutput_items;
}

template class spreading<float>;
template class spreading<gr_complex>;
template class spreading<uint8_t>;

} /* namespace sidloc */
} /* namespace gr */
