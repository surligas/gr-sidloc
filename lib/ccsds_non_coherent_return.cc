/* -*- c++ -*- */
/*
 * Copyright 2022 gr-sidloc author.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include <gnuradio/sidloc/ccsds_non_coherent_return.h>

namespace gr {
namespace sidloc {

/**
 * @brief Creates a shared pointer to a CCSDS non-coherent return code object that is
 * based on Gold sequences
 *
 * \ref https://public.ccsds.org/Pubs/415x1b1.pdf section 5.3.3
 *
 * @param seed_A the seed for the A polyonym
 * @param seed_C the seed for the A polyonym
 * @return code::sptr to a CCSDS non-coherent return code object
 */
code::sptr ccsds_non_coherent_return::make_shared(uint32_t seed_A, uint32_t seed_C)
{
    return code::sptr(new ccsds_non_coherent_return(seed_A, seed_C));
}

ccsds_non_coherent_return::ccsds_non_coherent_return(uint32_t seed_A, uint32_t seed_C)
    : code(2),
      m_seed_A(seed_A),
      m_seed_C(seed_C),
      m_a(0x201, seed_A, reg_len - 1),
      m_b(0x541, 0x1, reg_len - 1),
      m_c(0x201, seed_C, reg_len - 1),
      m_cnt(0)
{
}

ccsds_non_coherent_return::~ccsds_non_coherent_return() {}

void ccsds_non_coherent_return::reset()
{
    std::lock_guard lock(m_mtx);
    reset_unlocked();
}

size_t ccsds_non_coherent_return::length() const { return (1 << reg_len) - 1; }

void ccsds_non_coherent_return::next(std::vector<bool>& x)
{
    std::lock_guard lock(m_mtx);
    uint8_t b = m_b.next_bit();
    x[0] = m_a.next_bit() ^ b;
    x[1] = m_c.next_bit() ^ b;
    m_cnt++;
    if (m_cnt == length()) {
        reset_unlocked();
    }
}

void ccsds_non_coherent_return::period(std::vector<bool>& seq, size_t channel)
{
    if (channel > 1) {
        throw std::invalid_argument(
            "ccsds_non_coherent_return: Code supports 2 channels");
    }

    ccsds_non_coherent_return code(m_seed_A, m_seed_C);
    for (size_t i = 0; i < length(); i++) {
        std::vector<bool> x = { 0, 0 };
        code.next(x);
        seq.push_back(x[channel]);
    }
}

void sidloc::ccsds_non_coherent_return::reset_unlocked()
{
    m_a.reset();
    m_b.reset();
    m_c.reset();
    m_cnt = 0;
}

} /* namespace sidloc */
} /* namespace gr */
