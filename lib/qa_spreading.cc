/* -*- c++ -*- */
/* gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/attributes.h>
#include <gnuradio/sidloc/spreading.h>
#include <boost/test/unit_test.hpp>

namespace gr {
namespace sidloc {

BOOST_AUTO_TEST_CASE(test_spreading_replace_with_specific_test_name)
{
    // Put test here
}

} /* namespace sidloc */
} /* namespace gr */
