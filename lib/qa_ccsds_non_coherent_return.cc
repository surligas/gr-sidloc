/* -*- c++ -*- */
/* gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/attributes.h>
#include <gnuradio/sidloc/ccsds_non_coherent_return.h>
#include <boost/test/unit_test.hpp>

namespace gr {
namespace sidloc {

BOOST_AUTO_TEST_CASE(test_ccsds_non_coherent_return_test_length)
{
    ccsds_non_coherent_return code = ccsds_non_coherent_return(0, 0);
    BOOST_REQUIRE(code.channels() == 2);
    BOOST_REQUIRE(code.length() == 2047);
}

BOOST_AUTO_TEST_CASE(test_ccsds_non_coherent_return_first_bits)
{
    /* Just a simple test case */
    ccsds_non_coherent_return code(0, 0);
    std::vector<bool> bits(code.channels(), 0);
    code.next(bits);
    BOOST_REQUIRE(bits[0] == 1 && bits[1] == 1);
}

BOOST_AUTO_TEST_CASE(test_ccsds_non_coherent_return_cmp)
{
    /* Just a simple test case */
    ccsds_non_coherent_return code0(0, 0);
    ccsds_non_coherent_return code1(0, 0);
    std::vector<bool> bits0(code0.channels(), 0);
    std::vector<bool> bits1(code1.channels(), 0);

    for(size_t i = 0; i < code0.length(); i++) {
        code0.next(bits0);
        code1.next(bits1);
        BOOST_REQUIRE(bits0 == bits1);
    }
}

BOOST_AUTO_TEST_CASE(test_ccsds_non_coherent_return_code_check)
{
    /* Just a simple test case */
    ccsds_non_coherent_return c(0xAA, 0x33);
    std::vector<bool> ch0;
    std::vector<bool> ch1;
    c.period(ch0, 0);
    c.period(ch1, 1);
    BOOST_REQUIRE_EQUAL(ch0.size(), c.length());
    BOOST_REQUIRE_EQUAL(ch1.size(), c.length());
    BOOST_CHECK_THROW(c.period(ch0, 2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(test_ccsds_non_coherent_return_wrap)
{
    /* Just a simple test case */
    ccsds_non_coherent_return code0(0, 0);
    ccsds_non_coherent_return code1(0, 0);
    std::vector<bool> bits0(code0.channels(), 0);
    std::vector<bool> bits1(code1.channels(), 0);

    for(size_t i = 0; i < code0.length(); i++) {
        code0.next(bits0);
    }

    for(size_t i = 0; i < code0.length(); i++) {
        code0.next(bits0);
        code1.next(bits1);
        BOOST_REQUIRE_EQUAL(bits0 == bits1, true);
    }
}

BOOST_AUTO_TEST_CASE(test_ccsds_non_coherent_return_code_check_gold)
{
    /* A and C should produce the same output if the same seed is used */
    ccsds_non_coherent_return c(0xAA, 0xAA);
    std::vector<bool> ch0;
    std::vector<bool> ch1;
    c.period(ch0, 0);
    c.period(ch1, 1);
    BOOST_REQUIRE_EQUAL(ch0 == ch1, true);
}

} /* namespace sidloc */
} /* namespace gr */
