/* -*- c++ -*- */
/* gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_SPREADING_H
#define INCLUDED_SIDLOC_SPREADING_H

#include <gnuradio/sidloc/api.h>
#include <gnuradio/sidloc/code.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace sidloc {


template <typename T>
/*!
 * \brief This block performs the spreading of a frame with one of the supported spreading
 * sequences. The block can adapt to the characteristics of the code (e.g number of
 * channels) and the user needs (e.g interleaved channels, custom scaling factor).
 *
 * \ingroup sidloc
 *
 */
class SIDLOC_API spreading : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<spreading> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of sidloc::spreading.
     *
     * To avoid accidental use of raw pointers, sidloc::spreading's
     * constructor is in a private implementation
     * class. sidloc::spreading::make is the public interface for
     * creating new instances.
     * @param c the spreading code to use
     * @param scale the scaling factor to be applied. The basic code is bound in the
     * [-1,1] range and then the scaling factor is applied
     * @param channel_interleaving if set to true and the code supports multiple channels
     * the block produces a single output channel with each one of the code channels interleaved
     */
    static sptr make(code::sptr c, T scale, bool channel_interleaving = false);
};


template <>
/*!
 * \brief This block performs the spreading of a frame with one of the supported spreading
 * sequences. The block can adapt to the characteristics of the code (e.g number of
 * channels) and the user needs (e.g interleaved channels). For byte data type the
 * spreading output is bounded in the [0,1] range.
 *
 */
class SIDLOC_API spreading<uint8_t> : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<spreading> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of sidloc::spreading.
     *
     * To avoid accidental use of raw pointers, sidloc::spreading's
     * constructor is in a private implementation
     * class. sidloc::spreading::make is the public interface for
     * creating new instances.
     */
    static sptr make(code::sptr c, bool channel_interleaving = false);
};


using spreading_f = spreading<float>;
using spreading_c = spreading<gr_complex>;
using spreading_b = spreading<uint8_t>;

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_SPREADING_H */
