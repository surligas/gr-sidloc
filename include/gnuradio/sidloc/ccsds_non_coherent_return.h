/* -*- c++ -*- */
/*
 * Copyright 2022 gr-sidloc author.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_CCSDS_NON_COHERENT_RETURN_H
#define INCLUDED_SIDLOC_CCSDS_NON_COHERENT_RETURN_H

#include <gnuradio/digital/lfsr.h>
#include <gnuradio/sidloc/api.h>
#include <gnuradio/sidloc/code.h>
#include <mutex>

namespace gr {
namespace sidloc {

/*!
 * \brief Spreading code based on the CCSDS non-coherent return PN code (CCSDS 415.1-B-1)
 *
 */
class SIDLOC_API ccsds_non_coherent_return : public code
{
public:
    static constexpr size_t reg_len = 11;

    static code::sptr make_shared(uint32_t seed_A, uint32_t seed_C);

    ccsds_non_coherent_return(uint32_t seed_A, uint32_t seed_C);
    ~ccsds_non_coherent_return();

    size_t length() const;

    void next(std::vector<bool> &x);

    void reset();

    void period(std::vector<bool> &seq, size_t channel);

private:
    uint32_t m_seed_A;
    uint32_t m_seed_C;
    digital::lfsr m_a;
    digital::lfsr m_b;
    digital::lfsr m_c;
    size_t m_cnt;
    std::mutex m_mtx;

    void reset_unlocked();
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_CCSDS_NON_COHERENT_RETURN_H */
