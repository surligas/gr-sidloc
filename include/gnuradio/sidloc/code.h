/* -*- c++ -*- */
/* gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_CODE_H
#define INCLUDED_SIDLOC_CODE_H

#include <gnuradio/sidloc/api.h>
#include <bitset>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <memory>

namespace gr {
namespace sidloc {

/*!
 * \brief Base class describing a spreading code
 *
 */
class SIDLOC_API code
{
public:
    code(size_t channels) :m_channels(channels) {}
    ~code() {}

    using sptr = std::shared_ptr<code>;
    using uptr = std::unique_ptr<code>;

    /**
     * @brief Returns the number of bits of the spreading code until it repeats.
     * @note The length refers to the repeat period of each channel. For example if the sequence supports 2 channels with a period of 207 bits, the method will return 2047
     *
     * @return size_t the nunber of bits until  a repetition of the code occurs
     */
    virtual size_t length() const = 0;

    /**
     * @brief Returns the number of the supported channels
     *
     * @return constexpr size_t the supported channels
     */
    constexpr size_t channels() { return m_channels; }

    /**
     * @brief This method retrieves the next bit for each of the available channels
     *
     * @param x is a vector with a size equals the supported channels. The x[0] constains
     * the bit of the first channel, the x[1] the second e.t.c.
     */
    virtual void next(std::vector<bool>& x) = 0;

    /**
     * @brief Retrieves a whole period of the spreading sequence for a specific channel.
     * @note This method does not affect the internal state of the sequence in any way
     *
     * @param seq vector to append the sequence
     * @param channel the channel of interest
     */
    virtual void period(std::vector<bool> &seq, size_t channel) = 0;

    /**
     * @brief Resets everything to the initial state
     *
     */
    virtual void reset() = 0;
private:
    const size_t m_channels;

};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_CODE_H */
