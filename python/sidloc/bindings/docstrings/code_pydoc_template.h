/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,sidloc, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 
 static const char *__doc_gr_sidloc_code = R"doc()doc";


 static const char *__doc_gr_sidloc_code_code_0 = R"doc()doc";


 static const char *__doc_gr_sidloc_code_code_1 = R"doc()doc";


 static const char *__doc_gr_sidloc_code_length = R"doc()doc";


 static const char *__doc_gr_sidloc_code_channels = R"doc()doc";


 static const char *__doc_gr_sidloc_code_next = R"doc()doc";


 static const char *__doc_gr_sidloc_code_period = R"doc()doc";


 static const char *__doc_gr_sidloc_code_reset = R"doc()doc";

  
