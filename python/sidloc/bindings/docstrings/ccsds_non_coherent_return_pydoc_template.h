/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,sidloc, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */

 static const char *__doc_gr_sidloc_ccsds_non_coherent_return = R"doc()doc";


 static const char *__doc_gr_sidloc_ccsds_non_coherent_return_ccsds_non_coherent_return = R"doc()doc";


 static const char *__doc_gr_sidloc_ccsds_non_coherent_return_make_shared = R"doc()doc";


 static const char *__doc_gr_sidloc_ccsds_non_coherent_return_next = R"doc()doc";


 static const char *__doc_gr_sidloc_ccsds_non_coherent_return_reset = R"doc()doc";


 static const char *__doc_gr_sidloc_ccsds_non_coherent_return_period = R"doc()doc";


